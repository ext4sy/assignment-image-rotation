#include "image_rotate.h"
#include "image_manage.h"
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

bool rotate90(struct image* img) {
    if (img == NULL) {
        fprintf(stderr, "Invalid argument(s): %s\n", __FUNCTION__);
        return false;
    }
    struct image img_copy = img_create(img->width, img->height);
    if (img_copy.data == NULL) {
        fprintf(stderr, "Creating img_copy failed: %s\n", __FUNCTION__);
        return false;
    }


    for (size_t i = 0; i < img->width * img->height; ++i) {
        img_copy.data[i] = img->data[i]; 
    }

    for (size_t i = 0; i < img->height; ++i) {
        for (size_t j = 0; j < img->width; ++j) {
            img->data[(img->height - i - 1) + j * img->height] = img_copy.data[i*img->width + j];
        }
    }
    img->width = img_copy.height;
    img->height = img_copy.width;
    img_destroy(&img_copy);

    return true;
}
